import $ from 'jquery';
import Swiper from 'swiper';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';
import '../../node_modules/jquery-validation/dist/jquery.validate.min';





// Search Field
let searchField = document.getElementById('search');

searchField.addEventListener('focus',function () {
    this.parentElement.classList.add('active');
});
searchField.addEventListener('blur', function () {
  this.parentElement.classList.remove('active');
});


// SCROLL
let elHeader = document.getElementById('header'),
    elFirstScreen = document.getElementById('first-screen');

window.addEventListener('scroll', toggleClassOnScroll);

function toggleClassOnScroll() {
  if(document.documentElement.scrollTop > 50) {
    elHeader.classList.add('header--scroll');
    elFirstScreen.classList.add('first-screen--scroll');

  } else {
    elHeader.classList.remove('header--scroll');
    elFirstScreen.classList.remove('first-screen--scroll');
  }
}

// SEARCH

let searchMobileButton = document.querySelector('.mobile-header__search'),
    searchMobileButtonClose = document.querySelector('.mobile-header__search-input--close'),
    searchMobileField = document.querySelector('.mobile-header__search-input');

const addMobileField = () => {
  searchMobileField.classList.add('mobile-header__search-input--active');
}

const removeMobileFieldClose = () => {
  searchMobileField.classList.remove('mobile-header__search-input--active');
}

searchMobileButton.addEventListener('click', e => {
  e.stopPropagation();
  addMobileField();
});

searchMobileButtonClose.addEventListener('click', e => {
  e.stopPropagation();
  removeMobileFieldClose();
});

document.addEventListener('click', e => {
  let target = e.target;
  let its_searchMobileField = target === searchMobileField || searchMobileField.contains(target);
  let its_searchMobileButton = target === searchMobileButton;
  let searchMobileField_is_active = searchMobileField.classList.contains('mobile-header__search-input--active');

  if (!its_searchMobileField && !its_searchMobileButton && searchMobileField_is_active) {
    removeMobileFieldClose();
  }
});



// MOBILE drop menu
let dropMobileButton = document.querySelector('.mobile-header__menu-btn'),
  dropMobileButtonClose = document.querySelector('.mobile-menu__close-btn'),
  dropMobileMenu = document.querySelector('.mobile-header__drop-menu'),
  dropMobileLink = document.querySelector('.mobile-menu__menu'),
  dropMobileAnchor = document.querySelector('.mobile-menu__anchor');

const addMobileMenu = () => {
  dropMobileMenu.classList.add('mobile-header__drop-menu--active');
}

const removeMobileMenuClose = () => {
  dropMobileMenu.classList.remove('mobile-header__drop-menu--active');
}

dropMobileButton.addEventListener('click', e => {
  e.stopPropagation();
  addMobileMenu();
});

dropMobileButtonClose.addEventListener('click', e => {
  e.stopPropagation();
  removeMobileMenuClose();
});

dropMobileLink.addEventListener('click', e => {
  e.stopPropagation();
  removeMobileMenuClose();
});

dropMobileAnchor.addEventListener('click', e => {
  e.stopPropagation();
  removeMobileMenuClose();
});

document.addEventListener('click', e => {
  let target = e.target;
  let its_dropMobileMenu = target === dropMobileMenu || dropMobileMenu.contains(target);
  let its_dropMobileButton = target === dropMobileButton;
  let dropMobileMenu_is_active = dropMobileMenu.classList.contains('mobile-header__drop-menu--active');

  if (!its_dropMobileMenu && !its_dropMobileButton && dropMobileMenu_is_active) {
    removeMobileMenuClose();
  }
});


// Close mobile menu if window > 991px
$(window).resize(function () {
  if (window.innerWidth > 991) {
    $('.mobile-header__drop-menu').removeClass('mobile-header__drop-menu--active')
  }
});






//add swiper in first-screen

var swiperFirstScreen = new Swiper('.first-screen .swiper-container', {
  autoHeight: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    991: {
      pagination: {
        el: '.swiper-pagination',
      },
    }
  }
});


var swiperProducts = new Swiper('.products__slider .swiper-container', {
  slidesPerView: 4,
  spaceBetween: 12,
  autoHeight: true,
  navigation: {
    nextEl: '.products-button-next',
    prevEl: '.products-button-prev',
  }, breakpoints: {
    1199: {
      slidesPerView: 3,
      spaceBetween: 32,
    },
    991: {
      slidesPerView: 2,
      spaceBetween: 52,
    },
    767: {
      slidesPerView: 1,
      spaceBetween: 52,
    }
  }
});

var swiperArticles = new Swiper('.articles__slider .swiper-container', {
  slidesPerView: 4,
  spaceBetween: 12,
  autoHeight: true,
  navigation: {
    nextEl: '.articles-button-next',
    prevEl: '.articles-button-prev',
  }, breakpoints: {
    1199: {
      slidesPerView: 3,
      spaceBetween: 32,
    },
    991: {
      slidesPerView: 2,
      spaceBetween: 52,
    },
    767: {
      slidesPerView: 1,
      spaceBetween: 52,
    }
  }
});



var swiper = new Swiper('.info-content__slider .swiper-container', {
  speed: 600,
  allowTouchMove: false,
  loop: false,
});

var swiper2 = new Swiper('.info-content__nav .swiper-container', {
  speed: 600,
  loop: false,
  slidesPerView: 5,
  shortSwipes: false,
  spaceBetween: 5,

});

var activeYear;

$('.info-content__nav .swiper-slide').on('click', function() {
  $('.info-content__nav .swiper-slide').removeClass('sm-active-slide');
  $(this).addClass('sm-active-slide');

  var navIndex = $(this).index();

  swiper.slideTo(navIndex); // slide main slides to current nav item
  swiper2.slideTo(navIndex); // slide nav item to current nav item

});



$(document).ready(function () {

  $('.button--basket').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('button--checked');
  });
  $('.products__nav a').click(function (e) {
    e.preventDefault();
    $('.products__nav a').removeClass('active');
    $(this).addClass('active')
  });
  // Toggle NAV

  $('.mobile-menu__sub-btn').click(function () {
    $(this).next().addClass('mobile-menu__sub-menu--active')
  });
  $('.mobile-menu__back').click(function () {
    $(this).parent().removeClass('mobile-menu__sub-menu--active')
  });
  $('.modal').popup({
    transition: 'all 0.3s',
    outline: true,
    focusdelay: 400,
    vertical: 'top',
    closebutton: true
  });
  $('input[type="checkbox"]').click(function () {
    if ($(this).parent().find('.modal__checkbox-text').hasClass('checked') === false) {
      $(this).parent().find('.modal__checkbox-text').addClass('checked');
    } else {
      $(this).parent().find('.modal__checkbox-text').removeClass('checked');
    }
  });



  $('input[type="tel"]').mask('+7 (000) 000-00-00');
  $.validator.addMethod('phoneno', function(phoneNumber, element) {
    return this.optional(element) || phoneNumber.match(/\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  }, 'Введите Ваш телефон');

  $('.form').each(function(index, el) {
    $(el).addClass('form-' + index);
    $('.form-' + index).validate({
      rules: {
        name: 'required',
        tel: 'required',
        agree: 'required',
        login: 'required',
        pass: 'required'
      },
      messages: {
        name: 'Введите Ваше имя',
        tel: 'Введите Ваш телефон',
        agree: 'Я согласен на обработку персональных даных',
        login: 'Введите Ваш логин',
        pass: 'Введите Ваш пароль'
      },
      submitHandler: function(form) {
        var t = $('.form-' + index).serialize();
        ajaxSend('.form-' + index, t);
      }
    });
  });

  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'sendmail.php',
      data: data,
      success: function() {
        $('.modal').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
      }
    });
  };


});








